class ListingsController < ApplicationController
  def show
    @listing = Listing.find(params[:id])


  end

  def index
    @listings = Listing.all
  end

  def new
    @listing = Listing.new
  end

  def edit
    @listing = Listing.find(params[:id])
  end

  def create
    @listing = Listing.new(listing_params)
    @listing.agent = Agent.find(params[:agent_id])


    if @listing.save
      redirect_to @listing
    else
      render 'new'
    end
  end

  def update
    @listing = Listing.find(params[:id])
    @listing.agent = Agent.find(params[:agent_id])

    @feat = params[:feature_id]
    if !@feat.nil?

      @feat.each do |feature|

        obj = Feature.find(feature.to_i)

        @listing.feature << obj.name

      end

    end

    if @listing.update(listing_params)
      redirect_to @listing
    else
      render 'edit'
    end
  end

  private
  def listing_params
    params.require(:listing).permit(:hid, :address , :price, :description, :agent_id, :feature_id=>[])
  end

end
