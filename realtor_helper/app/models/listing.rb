class Listing < ActiveRecord::Base
belongs_to :agent
has_and_belongs_to_many :feature
  validates :hid, presence: true
  validates :address, presence: true
  validates :price, presence: true,
  :numericality => { :greater_than_or_equal_to => 0 }
  validates :description, presence: true
end
