Rails.application.routes.draw do
  resources :features
  resources :agents
  resources :listings
    root 'welcome#index'
end
