class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :hid
      t.string :address
      t.string :price

      t.timestamps null: false
    end
  end
end
