class ChangePriceFormatInListings < ActiveRecord::Migration
  def up
    change_column :Listings, :price, :decimal
  end

  def down
    change_column :Listings, :price, :string
  end
end
